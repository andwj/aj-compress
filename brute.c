/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char byte;


#define TERMINATOR_CODE   0xFFF

#define MAX_WORD_LEN  16


#define BLOCK_SIZE  32768
byte input_buffer[BLOCK_SIZE];
int  input_codes [BLOCK_SIZE];

size_t input_length;


typedef struct {
	byte length;
	byte data[MAX_WORD_LEN];
} dict_word_t;

#define MAX_DICT  0xB00

dict_word_t dictionary[MAX_DICT];


//----------------------------------------------------------------------
// ANALYSIS LOGIC
//----------------------------------------------------------------------

#define MAX_PARTS  8
#define MAX_USAGE  0xFFFF

typedef unsigned short word_usage_t;

typedef struct word_part_s {
	byte ch;
	byte num_parts;  /* only valid if parts256 == NULL */
	byte locked;     /* cannot add new parts here */

	word_usage_t usage;

	struct word_part_s *parts[MAX_PARTS];
	struct word_part_s ** parts256;  /* pointer to 256 pointers */
} word_part_t;


word_part_t *root;


typedef struct span_s {
	size_t start;
	size_t end;   /* one past last character */

	struct span_s *left;
	struct span_s *right;
} span_t;

span_t *input_span;


span_t *Span_New(size_t start, size_t end) {
	span_t *sp = (span_t *)calloc(1, sizeof(span_t));
	if (sp == NULL) {
		fprintf(stderr, "ERROR: out of memory\n");
		exit(2);
	}
	sp->start = start;
	sp->end   = end;
	return sp;
}

void Span_Free(span_t *sp) {
	if (sp->left) {
		Span_Free(sp->left);
		sp->left = NULL;
	}
	if (sp->right) {
		Span_Free(sp->right);
		sp->right = NULL;
	}
	free((void *) sp);
}


void Span_Split(span_t *sp, size_t pos, size_t end) {
	if (sp->start >= sp->end) {
		return;
	}

	/* any overlap? */
	if (end <= sp->start || pos >= sp->end) {
		return;
	}

	/* covers whole span? */
	if (pos <= sp->start && end >= sp->end) {
		sp->start = sp->end;

		if (sp->left) {
			Span_Free(sp->left);
			sp->left = NULL;
		}
		if (sp->right) {
			Span_Free(sp->right);
			sp->right = NULL;
		}
		return;
	}

	/* recurse into children if we have them */
	if (sp->left != NULL) {
		Span_Split(sp->left,  pos, end);
		Span_Split(sp->right, pos, end);

		if (sp->left->start > sp->start) {
			sp->start = sp->left->start;
		}
		if (sp->right->end < sp->end) {
			sp->end = sp->right->end;
		}
		return;
	}

	/* touches left side? */
	if (pos <= sp->start) {
		sp->start = end;
		return;
	}

	/* touches right side? */
	if (end >= sp->end) {
		sp->end = pos;
		return;
	}

	/* solid area is in middle, need to split */
	sp->left  = Span_New(sp->start, pos);
	sp->right = Span_New(end, sp->end);
}


static word_part_t *WordPart_New(byte ch) {
	word_part_t *wp = (word_part_t *)calloc(1, sizeof(word_part_t));
	if (wp == NULL) {
		fprintf(stderr, "ERROR: out of memory\n");
		exit(2);
	}
	wp->ch = ch;
	return wp;
}


static void WordPart_Free(word_part_t *wp) {
	size_t i;
	for (i = 0 ; i < MAX_PARTS ; i++) {
		if (wp->parts[i] != NULL) {
			WordPart_Free(wp->parts[i]);
			wp->parts[i] = NULL;
		}
	}

	if (wp->parts256 != NULL) {
		for (i = 0 ; i < 256 ; i++) {
			if (wp->parts256[i] != NULL) {
				WordPart_Free(wp->parts256[i]);
			}
		}
		free((void *) wp->parts256);
	}

	wp->parts256 = NULL;
	wp->num_parts = 0;

	free((void *) wp);
}


static void WordPart_Expand(word_part_t *wp) {
	if (wp->parts256 != NULL) {
		return;
	}

	wp->parts256 = (word_part_t **) calloc(256, sizeof(word_part_t *));
	if (wp->parts256 == NULL) {
		fprintf(stderr, "ERROR: out of memory\n");
		exit(2);
	}

	/* move any existing parts */

	size_t i;
	for (i = 0 ; i < MAX_PARTS ; i++) {
		word_part_t *child = wp->parts[i];
		if (child != NULL) {
			wp->parts256[child->ch] = child;
			wp->parts[i] = NULL;
		}
	}

	wp->num_parts = 0;
}


static void WordPart_RawAdd(const byte *data, size_t length) {
	word_part_t *wp = root;

	while (length > 0) {
		byte ch = *data++; length--;

		if (! wp->parts256) {
			/* check if `ch` already exists */
			size_t i;
			for (i = 0 ; i < MAX_PARTS ; i++) {
				word_part_t *child = wp->parts[i];
				if (child && child->ch == ch) {
					break;
				}
			}

			if (i < MAX_PARTS) {
				wp = wp->parts[i];
				continue;
			}

			/* need to create a new part */

			if (wp->locked) {
				/* silently fail when this is locked */
				return;
			}

			if (wp->num_parts < MAX_PARTS) {
				word_part_t *child = WordPart_New(ch);
				wp->parts[wp->num_parts] = child;
				wp->num_parts++;

				wp = child;
				continue;
			}

			/* we ran out of simple slots, so expand to full range */
			WordPart_Expand(wp);
		}

		if (wp->parts256[ch] == NULL) {
			/* need to create a new part */

			if (wp->locked) {
				/* silently fail when this is locked */
				return;
			}

			word_part_t *child = WordPart_New(ch);
			wp->parts256[ch] = child;
		}

		wp = wp->parts256[ch];
	}

	if (wp->usage < MAX_USAGE) {
		wp->usage++;
	}
}


static void WordPart_RawPrune(word_part_t *wp, size_t length) {
	size_t i;

	/* reached the length we are interested in? */
	if (length > 1) {
		/* nope, so recursively visit all sub-trees */
		length--;

		if (wp->parts256 != NULL) {
			for (i = 0 ; i < 256 ; i++) {
				if (wp->parts256[i] != NULL) {
					WordPart_RawPrune(wp->parts256[i], length);
				}
			}
		} else {
			for (i = 0 ; i < MAX_PARTS ; i++) {
				if (wp->parts[i] != NULL) {
					WordPart_RawPrune(wp->parts[i], length);
				}
			}
		}
		return;
	}

	/* yep, so remove any singletons */

	if (wp->parts256 != NULL) {
		for (i = 0 ; i < 256 ; i++) {
			word_part_t *child = wp->parts256[i];

			if (child != NULL && child->usage < 2) {
				WordPart_Free(child);
				wp->parts256[i] = NULL;
			}
		}
	} else {
		for (i = 0 ; i < MAX_PARTS ; i++) {
			word_part_t *child = wp->parts[i];

			if (child != NULL && child->usage < 2) {
				WordPart_Free(child);
				wp->parts[i] = NULL;
				wp->num_parts--;
			}
		}
	}

	/* prevent further additions to this node */
	wp->locked = 1;
}


void Words_Free(void) {
	if (root != NULL) {
		WordPart_Free(root);
		root = NULL;
	}
}


void Words_Init(void) {
	root = WordPart_New(0 /* dummy char */);

	WordPart_Expand(root);
}


void Words_Add(const byte *data, size_t length) {
	if (length > MAX_WORD_LEN) {
		length = MAX_WORD_LEN;
	}

	WordPart_RawAdd(data, length);
}


void Words_Prune(size_t length) {
	WordPart_RawPrune(root, length);
}


void Words_ProcessLength(span_t *sp, const byte *buf, size_t word_len) {
	if (sp->start + word_len > sp->end) {
		return;
	}

	if (sp->left != NULL) {
		Words_ProcessLength(sp->left,  buf, word_len);
		Words_ProcessLength(sp->right, buf, word_len);
		return;
	}

	size_t i;
	for (i = sp->start ; i + word_len <= sp->end ; i++) {
		Words_Add(buf + i, word_len);
	}
}


/* the idea here is we use Add() on all words of length 1 in the input
 * buffer, then prune away anything only used once, then use Add() for
 * all words of length 2 -- but inhibiting words starting with anything
 * previously pruned.  we then prune the new length, and repeat the
 * process for all words of length 3, and so forth....
 *
 * hence we end up with a tree of words which are used at least twice,
 * and the tree hasn't ballooned to a massive size by simply adding
 * every word of every length to it.
 */
void Words_ProcessBuffer(span_t *sp, const byte *buf) {
	size_t word_len;
	for (word_len = 1 ; word_len <= MAX_WORD_LEN ; word_len++) {
		Words_ProcessLength(sp, buf, word_len);
	}
}


/* compute the benefit (saved number of bits) of inserting
 * a word of the given length and usage into the dictionary
 * and emitting dictionary codes for it.
 *
 * we assume that the word being part of the dictionary has
 * an equivalent cost as the first time it is used -- in the
 * dictionary it has little overhead (it is stored as bytes),
 * whereas outside of the dictionary it would likely be a part
 * of a raw byte sequence.
 *
 * hence the benefit is really only from substituting later
 * usages with a dictionary code.
 */
int Words_CalcBenefit(size_t length, size_t usage) {
	int old_bits = (usage - 1) * length * 8;

	/* this was originally 'usage * 12', however the following
	 * equation produces much better results (I don't know why).
	 */
	int new_bits = (usage - 1) * 22;

	return old_bits - new_bits;
}


static void WordPart_Score(const word_part_t *wp, byte *cur_buf, size_t cur_len,
							int *best_score, byte *best_buf, size_t *best_len,
							size_t *best_usage) {

	byte save_ch = 0;

	if (wp != root) {
		save_ch = cur_buf[cur_len];

		/* build up the current word */
		cur_buf[cur_len] = wp->ch;
		cur_len++;

		if (cur_len >= 2 && wp->usage >= 2) {
			int score = Words_CalcBenefit(cur_len, wp->usage);

			if (score > *best_score) {
				*best_score = score;
				*best_len   = cur_len;
				*best_usage = wp->usage;

				memcpy(best_buf, cur_buf, MAX_WORD_LEN);
			}
		}
	}

	/* recurse into children */
	size_t i;

	if (wp->parts256 != NULL) {
		for (i = 0 ; i < 256 ; i++) {
			word_part_t *child = wp->parts256[i];
			if (child != NULL) {
				WordPart_Score(child, cur_buf, cur_len, best_score, best_buf, best_len, best_usage);
			}
		}
	} else {
		for (i = 0 ; i < MAX_PARTS ; i++) {
			word_part_t *child = wp->parts[i];
			if (child != NULL) {
				WordPart_Score(child, cur_buf, cur_len, best_score, best_buf, best_len, best_usage);
			}
		}
	}

	if (wp != root) {
		cur_buf[cur_len] = save_ch;
	}
}


int Words_PickBest(byte *best_buf, size_t *best_len, size_t *best_usage) {
	int best_score = -1;
	byte cur_buf[MAX_WORD_LEN+1];

	/* recursively scan the word tree */
	WordPart_Score(root, cur_buf, 0, &best_score, best_buf, best_len, best_usage);

	return best_score;
}


static void WordPart_RawRemove(const byte *data, size_t length, size_t usage) {
	word_part_t *wp = root;
	size_t i;

	/* find the word... */
	while (length > 0) {
		byte ch = *data++; length--;

		if (wp->parts256 != NULL) {
			word_part_t *child = wp->parts256[ch];
			if (child == NULL) {
				/* word is not in tree */
				return;
			}
			wp = child;
		} else {
			for (i = 0 ; i < MAX_PARTS ; i++) {
				word_part_t *child = wp->parts[i];
				if (child != NULL && child->ch == ch) {
					wp = child;
					break;
				}
			}
			if (i >= MAX_PARTS) {
				/* word is not in tree */
				return;
			}
		}
	}

	if (usage < wp->usage) {
		wp->usage -= usage;
	} else {
		wp->usage = 0;
	}
}


void Words_Remove(const byte *data, size_t length, size_t usage) {
	/* we need to handle the word itself AND all shorter substrings */

	size_t sub_len;
	size_t k;

	for (sub_len = 2 ; sub_len <= length ; sub_len++) {
		for (k = 0 ; k <= length - sub_len ; k++) {
			WordPart_RawRemove(data + k, sub_len, usage);
		}
	}
}


void Words_SpanRemove(span_t *sp, const byte *buf, const byte *word, size_t word_len, int code) {
	if (sp->start + word_len > sp->end) {
		return;
	}

	if (sp->left != NULL) {
		Words_SpanRemove(sp->left,  buf, word, word_len, code);
		Words_SpanRemove(sp->right, buf, word, word_len, code);
		return;
	}

	size_t i, k;

	for (i = sp->start ; i + word_len <= sp->end ; ) {
		if (0 == memcmp(buf + i, word, word_len)) {
			for (k = i ; k < i + word_len ; k++) {
				input_codes[k] = code;
			}
			Span_Split(sp, i, i + word_len);
			i += word_len;
			continue;
		}

		i++;
	}
}


//----------------------------------------------------------------------

static int ReadBlock(void) {
	if (feof(stdin)) {
		return 0;
	}

	clearerr(stdin);

	input_length = fread(input_buffer, 1, BLOCK_SIZE, stdin);
	if (ferror(stdin)) {
		fprintf(stderr, "ERROR: file error reading input\n");
		exit(1);
	}

	if (input_length == 0) {
		return 0;
	}

	return 1;  /* ok */
}


int first_code = -1;

byte pending_buffer[512];

size_t pending_length = 0;


static void OutByte(byte ch) {
	static byte buffer[4];
	buffer[0] = ch;

	fwrite(buffer, 1, 1, stdout);
}

static void OutPendingByte(byte ch) {
	if (first_code < 0) {
		OutByte(ch);
		return;
	}

	if (pending_length >= sizeof(pending_buffer)) {
		fprintf(stderr, "PENDING BUFFER OVERFLOW\n");
		exit(2);
	}

	pending_buffer[pending_length++] = ch;
}

static void OutCode(int code) {
	if (first_code < 0) {
		/* wait for the second of a pair */
		first_code = code;
		return;
	}

	int first  = first_code;
	int second = code;

	byte triplet[3];

	triplet[0] = (byte)( ((first & 0xF00) >> 4) | ((second & 0xF00) >> 8));
	triplet[1] = (byte)(first  & 0x0FF);
	triplet[2] = (byte)(second & 0x0FF);

	fwrite(triplet, 3, 1, stdout);

	/* flush any pending bytes */
	if (pending_length > 0) {
		fwrite(pending_buffer, pending_length, 1, stdout);
		pending_length = 0;
	}

	first_code = -1;
}

static void OutFinish(void) {
	OutCode(TERMINATOR_CODE);

	/* check for half-finished pair */
	if (first_code >= 0) {
		OutCode(TERMINATOR_CODE);
	}

	fflush(stdout);
}

static void OutDictEntry(size_t e) {
	fwrite(dictionary[e].data, dictionary[e].length, 1, stdout);
}

static void OutDictionary() {
	size_t e = 0;
	size_t i, k;

	if (first_code < 0) {
		/* dummy code to ensure a whole triplet */
		OutCode(0xE00);
	}

	OutCode(0xF00);

	for (i = 0 ; i < MAX_DICT ; ) {
		size_t count  = 1;
		size_t length = dictionary[e + i].length;

		/* rest of dictionary is empty? */
		if (length == 0) {
			break;
		}

		/* find longest group of same length.
		   [ FIXME : sort dictionary to optimize this ]
		*/
		while (count < 15 && (i + count) < 256 &&
				dictionary[e + i + count].length == length) {
			count++;
		}

		byte header = (byte)(count << 4) | (byte)(length - 1);
		OutByte(header);

		for (k = 0 ; k < count ; k++) {
			OutDictEntry(e + i + k);
		}

		i += count;
	}

	OutByte(0);
}


static void ClearDictionary(void) {
	int c;
	for (c = 0 ; c < MAX_DICT ; c++) {
		dictionary[c].length = 0;
	}
}


static int AddDictEntry(const byte *word, size_t word_len) {
	int c;
	for (c = 0 ; c < MAX_DICT ; c++) {
		if (dictionary[c].length == 0) {
			dictionary[c].length = word_len;
			memcpy(dictionary[c].data, word, word_len);
			return c;
		}
	}

	/* no free slots */
	return -1;
}


static void CreateDictionary(void) {
	ClearDictionary();

	for (;;) {
		Words_Init();
		Words_ProcessBuffer(input_span, input_buffer);

		byte kk[MAX_WORD_LEN+1];
		size_t klen;
		size_t kuse;

		int score = Words_PickBest(kk, &klen, &kuse);
		if (score < 0) {
			break;
		}

/* DEBUG
		kk[klen] = 0;
		fprintf(stderr, "BEST = '%s'  score: %d\n", (char *)kk, score);
*/
		int code = AddDictEntry(kk, klen);
		if (code < 0) {
			break;  /* full */
		}

		Words_SpanRemove(input_span, input_buffer, kk, klen, code);
		Words_Free();
	}
}


static size_t ConvertPos(size_t pos) {
	int code = input_codes[pos];
	if (code >= 0) {
		OutCode(code);
		return dictionary[code].length;
	}

	/* determine length of uncompressed segment */
	size_t length = 1;
	size_t i;

	while (length < 255 && pos+length < input_length && input_codes[pos+length] < 0) {
		length++;
	}

	if (length == 1) {
		OutCode(0xB00 | (int)input_buffer[pos]);
	} else if (length == 2) {
		OutCode(0xC00 | (int)input_buffer[pos]);
		OutPendingByte(input_buffer[pos + 1]);
	} else if (length == 3) {
		OutCode(0xD00 | (int)input_buffer[pos]);
		OutPendingByte(input_buffer[pos + 1]);
		OutPendingByte(input_buffer[pos + 2]);
	} else {
		OutCode(0xE00 | (int)length);

		for (i = 0 ; i < length ; i++) {
			OutPendingByte(input_buffer[pos + i]);
		}
	}

	return length;
}


int main(void) {
	while (ReadBlock()) {
		/* handle very short files */
		if (input_length < 16) {
			OutCode(0xE00 | (int)input_length);
			OutCode(TERMINATOR_CODE);
			fwrite(input_buffer, input_length, 1, stdout);
			break;
		}

		size_t i;
		for (i = 0 ; i < input_length ; i++) {
			input_codes[i] = -1;
		}

		input_span = Span_New(0, input_length);

		CreateDictionary();
		OutDictionary();

		size_t pos = 0;
		while (pos < input_length) {
			pos += ConvertPos(pos);
		}

		Span_Free(input_span);
		input_span = NULL;
	}

	OutFinish();

	return 0;
}
