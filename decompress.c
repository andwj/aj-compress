/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char byte;


#define TERMINATOR_CODE   0xFFF


#define MAX_ENTRY  16

typedef struct {
	byte length;
	byte data[MAX_ENTRY];
} dict_entry_t;

#define MAX_DICT  0xB00

dict_entry_t dictionary[MAX_DICT];


static void OutByte(byte ch) {
	static byte buffer[4];
	buffer[0] = ch;

	fwrite(buffer, 1, 1, stdout);
}

static void OutFinish(void) {
	fflush(stdout);
}

static int InByte(void) {
	static byte buffer[4];

	if (fread(buffer, 1, 1, stdin) != 1) {
		fprintf(stderr, "ERROR: read error or unexpected EOF\n");
		exit(1);
	}

	return (int)buffer[0];
}

static void CopyRawBytes(size_t length) {
	static byte buffer[512];

	/* zero length can be useful as a NO-OP code... */
	if (length == 0)
		return;

	if (fread(buffer, length, 1, stdin) != 1) {
		fprintf(stderr, "ERROR: read error or unexpected EOF\n");
		exit(1);
	}

	fwrite(buffer, length, 1, stdout);
}


int second_code = -1;

static int InCode(void) {
	if (second_code >= 0) {
		int result = second_code;
		second_code = -1;
		return result;
	}

	byte triplet[3];

	if (fread(triplet, 3, 1, stdin) != 1) {
		fprintf(stderr, "ERROR: read error or unexpected EOF\n");
		exit(1);
	}

	int first  = (int)triplet[1] | ((int)(triplet[0] >> 4)   << 8);
	int second = (int)triplet[2] | ((int)(triplet[0] & 0x0F) << 8);

	second_code = second;
	return first;
}


static void ReadDictionary(size_t dest) {
	for (;;)
	{
		int header = InByte();

		/* zero terminates the page */
		if (header == 0)
			break;

		int count  = (header >> 4);
		int length = (header & 0x0F) + 1;
		int i, k;

		if (length == 0) {
			fprintf(stderr, "ERROR: invalid dict header: $%02x\n", header);
			exit(1);
		}

		for (i = 0 ; i < count ; i++) {
			dictionary[dest].length = (byte)length;

			for (k = 0 ; k < length ; k++) {
				dictionary[dest].data[k] = InByte();
			}

			dest++;

			/* if we go past the final valid code, wrap around to beginning */
			if (dest >= MAX_DICT) {
				dest = 0;
			}
		}
	}
}


static void OutputDictionary(int entry) {
	size_t i;
	size_t length = dictionary[entry].length;

	if (length == 0) {
		fprintf(stderr, "WARNING: unused dict entry $%03x\n", entry);
		return;
	}

	for (i = 0 ; i < length ; i++) {
		OutByte(dictionary[entry].data[i]);
	}
}


int main(void) {
	for (;;) {
		int code = InCode();

		/* is it terminator code? */
		if (code == TERMINATOR_CODE) {
			break;
		}

		/* add a dictionary page? */
		if ((code & 0xF00) == 0xF00) {
			size_t start = (code & 0x0FF) << 4;
			ReadDictionary(start);
			continue;
		}

		/* copy raw bytes to the output? */
		if ((code & 0xF00) == 0xE00) {
			size_t length = (code & 0x0FF);
			CopyRawBytes(length);
			continue;
		}

		/* output a single byte? */
		if ((code & 0xF00) == 0xB00) {
			OutByte(code & 0x0FF);
			continue;
		}
		if ((code & 0xF00) == 0xC00) {
			OutByte(code & 0x0FF);
			CopyRawBytes(1);
			continue;
		}
		if ((code & 0xF00) == 0xD00) {
			OutByte(code & 0x0FF);
			CopyRawBytes(2);
			continue;
		}

		/* output a dictionary entry */
		OutputDictionary(code);
	}

	OutFinish();

	return 0;
}
