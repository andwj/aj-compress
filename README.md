
README
======

This is a data compression system which I created from scratch
just for fun.  I was interested in making an encoding scheme which
was as simple as possible, something very easy to decompress, but
still achieving a "reasonable" amount of compression of text files
(especially source code), ideally under 50% of the original size.

What I came up with *does* actually work to compress text files.
Applying it to the C++ code from another of my projects, Eureka,
the brute-force compressor produces files 54% of their original
size (on average), the figure is 62% for the fast compressor.
These figures are close to what the UNIX `compress` program will
produce (when setting maxbits to 12), though `gzip` is still much
much better at around 30% of their original size.

The [ENCODING.txt](ENCODING.txt) document describes the encoding
scheme in use here.  It is based on the concept of finding byte
sequences which are repeated, storing them in a dictionary so
they can be output by a single 12-bit code.  Not unlike the LZW
compression scheme.

I built two compressors for this encoding scheme: the first one
in `brute.c` works by brute-force finding the best byte sequence
in a block to replace, and it is slow, really really slow, in
fact it is incredibly *mind-numbingly* slow.  There are glaciers
which move faster than this!

The second one in `fast.c` is *much* faster, but does not compress
as well as the brute-force method.  It works more like a LZW
compressor, building up a dictionary as it goes, but it also
prunes the dictionary of non-repeated words and picks the most
beneficial of the remaining words.


Comparison Table
----------------

The following table shows the results of using `aj-compress`
(the fast version), UNIX `compress` (default settings), and
`gzip` (highest compression factor).  The values are the final
compressed size, expressed as a percentage of the original size.

These files are from the "Calgary Corpus", a collection of files
commonly used for testing compression algorithms.  Also included
is the IWAD for the game DOOM 2, just for kicks (it actually
compressed better than I expected).

```
    file       size       aj-comp   compress  gzip
    --------------------------------------------------
    bib        111261     50%       41%       31%
    book1      768771     51%       41%       40%
    book2      610856     50%       41%       33%
    geo        102400     76%       75%       66%
    news       377109     56%       48%       38%
    obj1       21504      72%       65%       47%
    obj2       246814     58%       52%       33%
    paper1     53161      59%       47%       34%
    paper2     82199      54%       43%       36%
    pic        513216     21%       12%       10%
    progc      39611      59%       48%       33%
    progl      71646      48%       37%       22%
    progp      49379      48%       38%       22%
    trans      93695      49%       40%       20%
    doom2.wad  14604584   66%       61%       42%
```


Author and License
------------------

Andrew Apted, 2019.

The license is MIT-style, a permissive open source license.
See the [LICENSE.md](LICENSE.md) file for the complete text.

