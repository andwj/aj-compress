/* Copyright 2019 Andrew Apted.
 * Use of this code is governed by an MIT-style license.
 * See the accompanying "LICENSE.md" file for the full text.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char byte;


#define TERMINATOR_CODE   0xFFF

#define MAX_WORD_LEN  16


#define BLOCK_SIZE  (65536 * 4)
byte input_buffer[BLOCK_SIZE];
int  input_codes [BLOCK_SIZE];

size_t input_length;


typedef struct {
	size_t pos;
	size_t length;
	size_t usage;
	int    benefit;
	int    real_code;
	int    hash;
	int    hash_next;
} dict_word_t;

#define MAX_DICT  0xB00


#define MAX_ACTIVE_DICT  (BLOCK_SIZE / 4)

dict_word_t active_dict[MAX_ACTIVE_DICT];

int active_refs[MAX_ACTIVE_DICT];
int active_count = 0;

#define HASH_TABLE_LEN  1021
int hash_table[HASH_TABLE_LEN];


static int ReadBlock(void) {
	if (feof(stdin)) {
		return 0;
	}

	clearerr(stdin);

	input_length = fread(input_buffer, 1, BLOCK_SIZE, stdin);
	if (ferror(stdin)) {
		fprintf(stderr, "ERROR: file error reading input\n");
		exit(1);
	}

	if (input_length == 0) {
		return 0;
	}

	size_t i;
	for (i = 0 ; i < input_length ; i++) {
		input_codes[i] = -1;

/* DEBUG
		if (input_buffer[i] == '\n') {
			input_buffer[i] = '@';
		}
*/
	}

	return 1;  /* ok */
}


int first_code = -1;

byte pending_buffer[512];

size_t pending_length = 0;


static void OutByte(byte ch) {
	static byte buffer[4];
	buffer[0] = ch;

	fwrite(buffer, 1, 1, stdout);
}

static void OutPendingByte(byte ch) {
	if (first_code < 0) {
		OutByte(ch);
		return;
	}

	if (pending_length >= sizeof(pending_buffer)) {
		fprintf(stderr, "PENDING BUFFER OVERFLOW\n");
		exit(2);
	}

	pending_buffer[pending_length++] = ch;
}

static void OutCode(int code) {
	if (first_code < 0) {
		/* wait for the second of a pair */
		first_code = code;
		return;
	}

	int first  = first_code;
	int second = code;

	byte triplet[3];

	triplet[0] = (byte)( ((first & 0xF00) >> 4) | ((second & 0xF00) >> 8));
	triplet[1] = (byte)(first  & 0x0FF);
	triplet[2] = (byte)(second & 0x0FF);

	fwrite(triplet, 3, 1, stdout);

	/* flush any pending bytes */
	if (pending_length > 0) {
		fwrite(pending_buffer, pending_length, 1, stdout);
		pending_length = 0;
	}

	first_code = -1;
}

static void OutFinish(void) {
	OutCode(TERMINATOR_CODE);

	/* check for half-finished pair */
	if (first_code >= 0) {
		OutCode(TERMINATOR_CODE);
	}

	fflush(stdout);
}

static void OutDictEntry(const dict_word_t *dw) {
	const byte *data = input_buffer + dw->pos;

	fwrite(data, dw->length, 1, stdout);
}

static void OutDictionary(int new_count) {
	int i, k;

	if (new_count == 0) {
		return;
	}

	if (first_code < 0) {
		/* dummy code to ensure a whole triplet */
		OutCode(0xE00);
	}

	OutCode(0xF00);

	for (i = 0 ; i < new_count ; ) {
		const dict_word_t *dw = &active_dict[active_refs[i]];

		int count  = 1;
		int length = dw->length;

		/* find longest group of same length.
		 * we sorted the dictionary earlier to optimize this.
		 */
		while (count < 15 && (i + count) < new_count &&
				active_dict[active_refs[i + count]].length == (size_t)length) {
			count++;
		}

		byte header = (byte)(count << 4) | (byte)(length - 1);
		OutByte(header);

		for (k = 0 ; k < count ; k++) {
			OutDictEntry(&active_dict[active_refs[i + k]]);
		}

		i += count;
	}

	OutByte(0);
}


static int HashWord(const byte *word, size_t length) {
	if (length == 0) {
		return 0;
	} else if (length == 1) {
		return (int)word[0] & (int)0x7F;
	} else {
		length = length * 0x12345;
		length ^= (size_t)word[0] * (size_t)13;
		length ^= (size_t)word[1] * (size_t)229;

		return (int)(length % HASH_TABLE_LEN);
	}
}


static void ClearHashTable(void) {
	int i;
	for (i = 0 ; i < HASH_TABLE_LEN ; i++) {
		hash_table[i] = -1;
	}
}


static void InsertHashTable(int e) {
	int hash = active_dict[e].hash;

	active_dict[e].hash_next = hash_table[hash];
	hash_table[hash] = e;
}


static void RebuildHashTable(void) {
	ClearHashTable();

	int i;
	for (i = 0 ; i < MAX_ACTIVE_DICT ; i++) {
		active_dict[i].hash_next = -1;
	}

	for (i = 0 ; i < active_count ; i++) {
		int k = active_refs[i];
		InsertHashTable(k);
	}
}


static int AddActiveWord(size_t pos, size_t length) {
	// this generally won't happen, and if it does then nothing bad happens
	if (active_count >= MAX_ACTIVE_DICT) {
		return MAX_ACTIVE_DICT-1;
	}

	int e = active_count;
	active_count++;

	active_refs[e] = e;

	active_dict[e].pos = pos;
	active_dict[e].length = length;
	active_dict[e].usage = 0;
	active_dict[e].real_code = -1;

//fprintf(stderr, "AddActiveWord [%d] @ %d = '%.*s'\n", e, (int)pos, (int)length, (char *)(input_buffer + pos));

	// compute hash (only needs to be done once)
	active_dict[e].hash = HashWord(input_buffer+pos, length);

	InsertHashTable(e);

	return e;
}


static int WordInDict(const byte *word, size_t length) {
	int hash = HashWord(word, length);

	int k = hash_table[hash];

	while (k >= 0) {
		const dict_word_t *dw = &active_dict[k];

		if (length == (size_t)dw->length &&
		    0 == memcmp(word, input_buffer + dw->pos, length)) {

			return k;  /* return RAW active_dict[] index */
		}

		k = dw->hash_next;
	}
	return -1;  /* not found */
}


static void ProcessBlock(void) {
	size_t pos = 0;
	size_t length = 0;

	int code;

	while (pos+1 < input_length) {
		length = 2;

		int use_code = -1;

		/* find longest word NOT in dictionary */
		for (;;) {
			code = WordInDict(input_buffer+pos, length);
			if (code < 0) {
				break;
			}

			use_code = code;

			if (pos + length >= input_length) {
				active_dict[use_code].usage += 1;
				return;
			}

			length++;
		}

		if (use_code >= 0) {
			active_dict[use_code].usage += 1;
		}

		if (length <= MAX_WORD_LEN) {
			AddActiveWord(pos, length);
		}

		/* skip to the unmatch character */
		pos += length-1;
	}
}


static int CalcBenefit(size_t length, size_t usage) {
	if (usage == 0)
		return -999999;

	int old_bits = (usage - 1) * length * 8;

	/* this was originally 'usage * 12', however the following
	 * equation produces much better results (I don't know why).
	 */
	// int new_bits = (usage - 1) * 22;
	int new_bits = (usage - 0) * 11;

	return old_bits - new_bits;
}


static int benefit_sort_cmp(const void *A0, const void *B0) {
	int A1 = *(const int *)A0;
	int B1 = *(const int *)B0;

	const dict_word_t *A = &active_dict[A1];
	const dict_word_t *B = &active_dict[B1];

	/* sort into DESCENDING order of benefit value */
	return (B->benefit - A->benefit);
}


static int length_sort_cmp(const void *A0, const void *B0) {
	int A1 = *(const int *)A0;
	int B1 = *(const int *)B0;

	const dict_word_t *A = &active_dict[A1];
	const dict_word_t *B = &active_dict[B1];

	return ((int)A->length - (int)B->length);
}


static void ProcessDictionary(void) {
	int used = 0;
	int i;

	/* compute benefit scores */
	for (i = 0 ; i < active_count ; i++) {
		active_refs[i] = i;
		active_dict[i].benefit = CalcBenefit(active_dict[i].length, active_dict[i].usage);

		if (active_dict[i].usage >= 1) {
			used++;
		}
	}

//	fprintf(stderr, "active dict %d, used %d\n", active_count, used);
}


static void ProcessDictionary2(void) {
	int i;

	for (i = 0 ; i < active_count ; i++) {
		active_dict[active_refs[i]].real_code = -1;
	}

	/* sort active dictionary based on benefit score */

	if (active_count > 1) {
		qsort(&active_refs, active_count, sizeof(int), benefit_sort_cmp);
	}

/* DEBUG
for (i = 0 ; i < active_count ; i++) {
fprintf(stderr, "ref [%d] -> absolute %d :  benefit %d\n", i, active_refs[i],
		active_dict[active_refs[i]].benefit);
}
*/

	/* trim the useless crud */
	for (i = 0 ; i < active_count ; i++) {
		if (active_dict[active_refs[i]].benefit < 0) {
			active_count = i;
			break;
		}
	}

	/* truncate dictionary if too large, keep the most benefical entries */
	if (active_count > MAX_DICT)
		active_count = MAX_DICT;

	if (active_count > 1) {
		qsort(&active_refs, active_count, sizeof(int), length_sort_cmp);
	}

	/* assign the real_code values */
	for (i = 0 ; i < active_count ; i++) {
		active_dict[active_refs[i]].real_code = i;
	}
}


static void ProcessBlock2(void) {
	size_t pos = 0;
	size_t length = 0;

	int code;

	/* rebuild usage count */
	for (code = 0 ; code < active_count ; code++) {
		active_dict[active_refs[code]].usage = 0;
	}

	while (pos+1 < input_length) {
		/* find longest word *IN* dictionary */
		length = MAX_WORD_LEN;
		if (pos + length > input_length) {
			length = input_length - pos;
		}

		while (length >= 2) {
			code = WordInDict(input_buffer+pos, length);
			if (code >= 0) {
				input_codes[pos] = code;
				active_dict[code].usage += 1;
				break;
			}

			length--;
		}

		/* skip to the unmatch character */
		pos += length;
	}

	int i;
	for (i = 0 ; i < active_count ; i++) {
		dict_word_t *dw = &active_dict[active_refs[i]];
		if (dw->usage < 2) {
			dw->usage = 0;
			dw->benefit = -777777;
		}
	}

	/* remove dud codes from input_codes too */
	for (pos = 0 ; pos < input_length ; pos++) {
		int code = input_codes[pos];
		if (code >= 0) {
			const dict_word_t *dw = &active_dict[code];
			if (dw->benefit < 0) {
				input_codes[pos] = -1;
			}
		}
	}
}


static size_t ConvertPos(size_t pos) {
	int code = input_codes[pos];
	if (code >= 0) {
		const dict_word_t *dw = &active_dict[code];
		if (dw->real_code >= 0) {
			OutCode(dw->real_code);
			return dw->length;
		}
	}

	/* determine length of uncompressed segment */
	size_t length = 1;
	size_t i;

	while (length < 255 && pos+length < input_length && input_codes[pos+length] < 0) {
		length++;
	}

	if (length == 1) {
		OutCode(0xB00 | (int)input_buffer[pos]);
	} else if (length == 2) {
		OutCode(0xC00 | (int)input_buffer[pos]);
		OutPendingByte(input_buffer[pos + 1]);
	} else if (length == 3) {
		OutCode(0xD00 | (int)input_buffer[pos]);
		OutPendingByte(input_buffer[pos + 1]);
		OutPendingByte(input_buffer[pos + 2]);
	} else {
		OutCode(0xE00 | (int)length);

		for (i = 0 ; i < length ; i++) {
			OutPendingByte(input_buffer[pos + i]);
		}
	}

	return length;
}


int main(void) {
	while (ReadBlock()) {
		/* handle very short files */
		if (input_length < 16) {
			OutCode(0xE00 | (int)input_length);
			OutCode(TERMINATOR_CODE);
			fwrite(input_buffer, input_length, 1, stdout);
			break;
		}

		active_count = 0;

		ClearHashTable();

		ProcessBlock();
		ProcessDictionary();
		ProcessDictionary2();

		/* second pass over block, using optimal dictionary */
		RebuildHashTable();

		ProcessBlock2();
		ProcessDictionary2();

		OutDictionary(active_count);

		size_t pos = 0;
		while (pos < input_length) {
			pos += ConvertPos(pos);
		}
	}

	OutFinish();

	return 0;
}
