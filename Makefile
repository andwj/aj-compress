
all: brute fast decompress

clean:
	rm -f brute fast decompress

fast: fast.c
	$(CC) -g3 -Wall -Wextra fast.c -o $@

brute: brute.c
	$(CC) -g3 -Wall -Wextra brute.c -o $@

decompress: decompress.c
	$(CC) -g3 -Wall -Wextra decompress.c -o $@

